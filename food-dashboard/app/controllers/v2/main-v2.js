// khi user load trang

const BASE_URL = "https://63f620359daf59d1ad826785.mockapi.io";
let cart = [];

let renderProducList = (productArr, idTag) => {
  let contentHTML = "";
  productArr.forEach((item) => {
    contentHTML += ` 
                <div class="card text-center col-6 my-3 pt-3 ">
                  <img class="card-img-top w-25 m-auto" src=${item.img} alt="">
                  <div class="card-body">
                    <h4 class="card-title">${item.name}</h4>
                    <p class="card-text">${item.desc}</p>
                    <button onclick="addToCart(${item.id})"  class="btn btn-success">Add</button>
                    </div>
                    </div>
              `;
  });
  document.getElementById(idTag).innerHTML = contentHTML;
};

let renderCartList = (cartArr) => {
  let contentHTML = "";
  let result = 0;
  cartArr.forEach((item) => {
    contentHTML += ` 
      <tr>
          <td>${item.id}</td>
          <td>${item.name}</td>
          <td>$${item.price}</td>
          <td>
            <img style="width : 50px" src=${item.img} alt="" />
          </td>
          <td>
          <button onclick="changeQuantity(${item.id},-1)" class="btn btn-warning mx-0">-</button>
          <strong class="mx-2">${item.soLuong}</strong>
          <button onclick="changeQuantity(${item.id},1)" class="btn btn-danger mx-0">+</button>
          </td>
          <td >
          <button onclick="deleteProduct(${item.id})" class="btn  text-danger">
          <i class="fa fa-trash"></i>
          </button>
          </td>
          <td> 
            <strong>$${item.thanhTien}</strong>
            <button onclick="totalProduct(${item.id})" class="btn border-success text-success">Tính</button> 
          </td>
      </tr>
      
      `;
    result += item.thanhTien;
  });
  contentHTML += ` 
  
  <tr>
        
        <td style="font-size : 20px">
          Tổng tiền phải trả : 
        </td>
        <td style="font-size : 30px">
        <strong>$${result}</strong>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
  `;
  document.getElementById("tbody").innerHTML = contentHTML;
};

let fetchProductList = () => {
  axios({
    url: `${BASE_URL}/product`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      renderProducList(res.data, "result");
    })
    .catch((err) => {
      console.log(err);
    });
};

fetchProductList();

window.handleChangePhone = () => {
  axios({
    url: `${BASE_URL}/product`,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      let selectValue = document.getElementById("selLoai").value;
      if (selectValue == "iphone") {
        let iphoneArr = res.data.filter((item) => {
          return item.type == "iphone";
        });

        renderProducList(iphoneArr, "result");
      } else if (selectValue == "samsung") {
        let samsungArr = res.data.filter((item) => {
          return item.type == "Samsung";
        });
        renderProducList(samsungArr, "result");
      } else {
        renderProducList(res.data, "result");
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

window.addToCart = (id) => {
  console.log("id: ", id);
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      let { id, name, price, img } = res.data;
      let product = {
        id,
        name,
        price,
        img,
      };
      let index = cart.findIndex((item) => {
        return item.id == id;
      });
      if (index == -1) {
        let newProduct = { ...product, soLuong: 1, thanhTien: 0 };
        cart.push(newProduct);
      } else {
        cart[index].soLuong += 1;
      }
      renderCartList(cart);
    })
    .catch((err) => {
      console.log(err);
    });
  //   cart.push(phone);
  //   renderCartList(cart);
};

window.changeQuantity = (id, value) => {
  let index = cart.findIndex((item) => {
    return item.id == id;
  });
  cart[index].soLuong += value;
  cart[index].soLuong == 0 && cart.splice(index, 1);
  renderCartList(cart);
};

window.deleteProduct = (id) => {
  let index = cart.findIndex((item) => {
    return item.id == id;
  });
  cart.splice(index, 1);
  renderCartList(cart);
};

window.totalProduct = (id) => {
  let index = cart.findIndex((item) => {
    return item.id == id;
  });
  cart[index].thanhTien = cart[index].price * cart[index].soLuong;
  renderCartList(cart);
};

window.totalAllProduct = () => {
  let total = 0;
  cart.forEach((item) => {
    total += item.thanhTien;
  });
  return total;
};
