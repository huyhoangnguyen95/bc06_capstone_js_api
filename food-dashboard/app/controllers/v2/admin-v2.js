import { SanPham } from "./SanPham.js";
import { layThongTinTuForm, showInfo } from "./controller-v2.js";
import { kiemTraLoaiSanPham, kiemTraRong } from "./validate.js";

const BASE_URL = "https://63f620359daf59d1ad826785.mockapi.io";
const VALIDATE_STRING = "không được để rỗng";

let renderProducList = (productArr, idTag) => {
  let contentHTML = "";
  productArr.forEach((item) => {
    contentHTML += ` 
                 <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td>
                    <img style="width : 100px" src=${item.img} alt="" />
                    </td>
                    <td>${item.type}</td>
                    <td>
                    <button onclick="xoaSanPham(${item.id})" class="btn btn-danger">Delete</button>
                    <button onclick="xemChiTiet(${item.id})" class="btn btn-warning">View Detail</button>
                    </td>
                 </tr>
                `;
  });
  document.getElementById(idTag).innerHTML = contentHTML;
};

let fetchProductList = () => {
  axios({
    url: `${BASE_URL}/product`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      renderProducList(res.data, "tbody");
    })
    .catch((err) => {
      console.log(err);
    });
};

fetchProductList();

window.addProduct = () => {
  document.getElementById("btnCapNhat").disabled = true;
  document.getElementById("btnThemMon").disabled = false;
  resetForm();
};

window.themSanPham = () => {
  let { maSanPham, tenSanPham, loaiSanPham, giaSanPham, hinhSanPham } =
    layThongTinTuForm();
  let sanPham = new SanPham(
    maSanPham,
    tenSanPham,
    loaiSanPham,
    giaSanPham,
    hinhSanPham
  );
  // kiểm tra mã
  let isValid = kiemTraRong(sanPham.id, "invalidID", `Mã ${VALIDATE_STRING}`);

  // kiểm tra tên
  isValid &= kiemTraRong(sanPham.name, "invalidTen", `Tên ${VALIDATE_STRING}`);

  // kiểm tra giá
  isValid &= kiemTraRong(sanPham.price, "invalidGia", `Gía ${VALIDATE_STRING}`);

  // kiểm tra loại
  isValid &= kiemTraLoaiSanPham(sanPham.type);
  if (isValid) {
    axios({
      url: `${BASE_URL}/product`,
      method: "POST",
      data: sanPham,
    })
      .then((res) => {
        console.log(res);

        $("#exampleModal").modal("hide");
        fetchProductList();
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

window.xoaSanPham = (id) => {
  console.log("id: ", id);
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
      fetchProductList();
    })
    .catch((err) => {
      console.log(err);
    });
};

window.xemChiTiet = (id) => {
  console.log("id: ", id);
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      $("#exampleModal").modal("show");
      showInfo(res.data);
      document.getElementById("btnThemMon").disabled = true;
      document.getElementById("btnCapNhat").disabled = false;
    })
    .catch((err) => {
      console.log(err);
    });
};

window.capNhatSanPham = () => {
  let { maSanPham, tenSanPham, loaiSanPham, giaSanPham, hinhSanPham } =
    layThongTinTuForm();
  let newSanPham = new SanPham(
    maSanPham,
    tenSanPham,
    loaiSanPham,
    giaSanPham,
    hinhSanPham
  );
  axios({
    url: `${BASE_URL}/product/${newSanPham.id}`,
    method: "PUT",
    data: newSanPham,
  })
    .then((res) => {
      console.log(res);
      $("#exampleModal").modal("hide");
      fetchProductList();
    })
    .catch((err) => {
      console.log(err);
    });
};

let resetForm = () => {
  document.getElementById("foodForm").reset();
};
