let showMessage = (idErr, message) => {
  document.getElementById(idErr).innerHTML = `${message}`;
};

export let kiemTraRong = (value, idErr, message) => {
  let length = value.length;
  if (length != 0) {
    showMessage(idErr, "");
    return true;
  } else {
    document.getElementById(idErr).style.display = "block";
    showMessage(idErr, message);
    return false;
  }
};

export let kiemTraLoaiSanPham = (value) => {
  if (value != "") {
    showMessage("invalidLoai", "");
    return true;
  } else {
    document.getElementById("invalidLoai").style.display = "block";
    showMessage("invalidLoai", "Bạn chưa chọn loại sản phẩm");
    return false;
  }
};
