export class SanPham {
  constructor(ma, ten, loai, gia, hinh) {
    this.id = ma;
    this.name = ten;
    this.type = loai;
    this.price = gia;
    this.img = hinh;
  }
}
