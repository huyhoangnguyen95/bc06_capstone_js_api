export let layThongTinTuForm = () => {
  const maSanPham = document.getElementById("productID").value;
  const tenSanPham = document.getElementById("tenProduct").value;
  const loaiSanPham = document.getElementById("loai").value;
  const giaSanPham = document.getElementById("giaProduct").value;
  const hinhSanPham = document.getElementById("hinhProduct").value;
  return {
    maSanPham,
    tenSanPham,
    loaiSanPham,
    giaSanPham,
    hinhSanPham,
  };
};

export let showInfo = (product) => {
  document.getElementById("productID").value = product.id;
  document.getElementById("tenProduct").value = product.name;
  document.getElementById("loai").value = product.type;
  document.getElementById("giaProduct").value = product.price;
  document.getElementById("hinhProduct").value = product.img;
};
